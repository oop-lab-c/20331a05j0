//C++ program to compute arithmetic operations with the user input and choice of operation(switch case).
#include<iostream>
using namespace std;
int main()
{
    int a,b;//Declaring 2 integer variables to store the 2 operands
    cout<<"Enter operand 1:(any integer)"<<endl;
    cin>>a;//Taking operand 1 as input from the user
    cout<<"Enter operand 2:(any integer)"<<endl;
    cin>>b;//Taking operand 2 as input from the user
    cout<<"Enter any Arithmetic operator of your choice:\n Addition : '+' \n Subtraction : '-' \n Multiplication : '*' \n Division(To compute Quotient) : '/' \n Modulo Division(To compute Remainder) : '%'"<<endl;
    char op;
    cin>>op;
    switch (op)
    {
    case '+':
    {
        cout<<"Arithmetic operation choosen is addition\n"<<"Result is:"<<a+b<<endl;
        break;
    }
    case '-':
    {
        cout<<"Arithmetic operation choosen is subtraction\n"<<"Result is:"<<a-b<<endl;
        break;
    }
    case '*':
    {
        cout<<"Arithmetic operation choosen is multiplication\n"<<"Result is:"<<a*b<<endl;
        break;
    }
    case '/':
    {
        if(b==0)
        {
        //If denominator is zero, Quotient will not be computed
        cout<<"Division not possible...Denominator should not be zero"<<endl;
        break;
        }
        else
        {
        cout<<"Arithmetic operation choosen is division\n"<<"Quotient is:"<<a/b<<endl;
        break;
        }
    }
    case '%':
    {
        if(b==0)
        {
            //If Denominator is zero, then the Remainder will not be computed
            cout<<"Modulo Division not possible...Denominator should not be zero"<<endl;
            break;
        }
        else
        {
        cout<<"Arithmetic operation choosen is Modulo Division\n"<<"Remainder is:"<<a%b<<endl;
        break;
        }
    }
    default://If the operator doesn't match
    {
        cout<<"invalid operator"<<endl;
        break;
    }
    }
}