// Program to demonstrate the usage of access inheritence
#include<iostream>
using namespace std;
class Father
{
    private:
    int val = 2;
    protected:
    int var = 4;
    public:
    int num = 5;
    void pvt()
    {
        cout << "private value: " << val << endl;
    }
};
class Daughter : public Father
{
  public:
  void display()
  {
  cout << "public value: " << num << endl;
  cout << "protected : " << var << endl;
  //private cannot be accessed...
  }
};
class Mother
{
    private:
    int val1 = 1;
    protected:
    int var1 = 7;
    public:
    int num1 = 6;
    void pvt1()
    {
        cout << "private value: " << val1 << endl;
    }
};
class Son : protected Mother
{
  public:
  void display1()
  {
  cout << "public value: " << num1 << endl;
  cout << "protected : " << var1 << endl;
  //private cannot be accessed...
  }
};
class Grandson : protected Son
{
    public:
    void displaynew()
  {
  cout << "public value new: " << num1 << endl;
  cout << "protected new: " << var1 << endl;
  //private cannot be accessed...
  }
};
class parents
{
    private:
    int val2 = 3;
    protected:
    int var2 = 9;
    public:
    int num2 = 12;
    void pvt2()
    {
        cout << "private value: " << val2 << endl;
    }
};
class Child : private parents
{
  public:
  void display2()
  {
  cout << "public value: " << num2 << endl;
  cout << "protected : " << var2 << endl;
  //private cannot be accessed...
  }
};
int main()
{
    Daughter obj;
    cout << "PUBLIC INHERITENCE: " << endl;  //public inheritence
    obj.display();
    obj.pvt();
    Grandson obj1;
    Son ob;
    cout << "PROTECTED INHERITENCE: " << endl; //protected inheritence
    obj1.displaynew();
    ob.display1();
    Child obj2;
    cout << "PRIVATE INHERITENCE: " << endl; //private inheritence
    obj2.display2();
    return 0;
}